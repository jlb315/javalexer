default: Token.class Lexer.class Parser.class Test.class

Test.class: Parser.class Lexer.class Token.class Test.java
	javac -d . -classpath . Test.java

Parser.class: Lexer.class Token.class Parser.java
	javac -d . -classpath . Parser.java

Lexer.class: Token.class Lexer.java
	javac -d . -classpath . Lexer.java

Token.class: Token.java
	javac -d . -classpath . Token.java

clean: 
	rm -f *.class
