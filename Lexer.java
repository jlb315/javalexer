/* Jennifer Brown
 * jlb315
 * CSE 262
 * Program 1
 *
 * Java version 1.8
 * Compiled on Linux. makefile included.
 */

/* Class Lexer will parse input string into series of tokens */

public class Lexer{
	private String input;
	private char ch;
	private String currentLex;
	private int index;
	//constructor
	public Lexer(String in){
		this.input = in;
		this.index = 0;
		this.ch = nextChar();
		currentLex = "";
	}
	/* Returns next token from string. Reads input String one char at
	 * a time and determines token type based on the restricted SQL
	 * lexical syntax
	 */
	public Token nextToken(){
		currentLex = "";
		while(isWSpace(ch)){
			ch = nextChar();
		}
		//parse floats and ints
		if(Character.isDigit(ch)){
			add(ch);
			while(Character.isDigit(ch)){
				add(ch);
			}
			if(ch == '.'){
				add(ch);
				while(Character.isDigit(ch)){
					add(ch);
				}
				return new Token(Token.Type.FLOAT, currentLex); 
			} else {
				return new Token(Token.Type.INT, currentLex);
			}
		//parse keywords and id's
		} else if(Character.isLetter(ch)){
			add(ch);
			while(Character.isLetterOrDigit(ch) || ch == '_'){
				add(ch);
			}
			switch(currentLex){
				case "SELECT":
				case "select":
					return new Token(Token.Type.SELECT, currentLex);
				case "FROM":
				case "from":
					return new Token(Token.Type.FROM, currentLex);
				case "WHERE":
				case "where":
					return new Token(Token.Type.WHERE, currentLex);
				case "AND":
				case "and":
					return new Token(Token.Type.AND, currentLex);
				default:
					return new Token(Token.Type.ID, currentLex);
			}
		//parse special characters for EOI, OPERATOR, COMMA, ERROR
		} else {
			switch(ch){
				case '$':
					add(ch);
					return new Token(Token.Type.EOI, currentLex);
				case '=':
				case '<':
				case '>':
					add(ch);
					return new Token(Token.Type.OPERATOR, currentLex);
				case ',':
					add(ch);
					return new Token(Token.Type.COMMA, currentLex);
				default:
					add(ch);
					return new Token(Token.Type.ERROR, currentLex);
			}
		}
	}
	//Returns next char in input string
	public char nextChar(){
		char c;
		if(index == input.length()){
			c = '$';
		}else{
			c = input.charAt(index++);
		}
		return c;
	}
	//Adds current char to currentLexeme and advances to next char
	public void add(char c){
		currentLex += c;
		ch = nextChar();
	}
	//Returns true if c is white space
	public boolean isWSpace(char c){
		if(c == ' ' || c == '\n' || c == '\t'){
			return true;
		} else {
			return false;
		}
	}
	//Returns true if c is operator
	public boolean isOp(char c){
		if(c == '=' || c == '<' || c == '>'){
			return true;
		} else {
			return false;
		}
	}

}
