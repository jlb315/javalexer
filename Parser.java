/* Jennifer Brown
 * jlb315
 * CSE 262
 * Program 1
 *
 * Java version 1.8
 * Compiled in Linux. Make file included.
 */

/* Parser class parses tokens from Lexer using recursive descent
 * technique
 */

public class Parser{
	private Lexer lexer;
	private Token token;
	private int numTabs;
	private String tabs = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
	//constructor
	public Parser(String input){
		this.lexer = new Lexer(input);
		token = lexer.nextToken();
		numTabs = 0;
	}
	//check if token is of expected type.
	private void check(Token.Type tokenType){
		if(token.getType() != tokenType){
			System.out.println("Synatax error: expecting "+tokenType+"Found "+token.getValue());
			System.exit(1);
		}
	}
	//initializes parse
	public void run(){
		query();
	}
	//parsers query
	public void query(){
		System.out.println("<Query>");
		numTabs++;
		keyword(Token.Type.SELECT);
		idList();
		keyword(Token.Type.FROM);
		idList();
		if(token.getType() == Token.Type.WHERE){
			keyword(Token.Type.WHERE);
			condList();
		} else {
			System.out.println("Syntax Error: expecting "+Token.Type.WHERE+" Found "+token.getValue());
			System.exit(1);
		}
		numTabs--;
		System.out.println("</Query>");
	}
	//parses IdList
	public void idList(){
		System.out.println(tabs.substring(0,numTabs++)+"<IdList>");
		id();
		while(token.getType() == Token.Type.COMMA){
			comma();
			id();
		}
		System.out.println(tabs.substring(0,--numTabs)+"</IdList>");
	}
	//parses CondList
	public void condList(){
		System.out.println(tabs.substring(0,numTabs++)+"<CondList>");
		cond();
		while(token.getType() == Token.Type.AND) {
			keyword(Token.Type.AND);
			cond();
		}
		System.out.println(tabs.substring(0,--numTabs)+"</CondList>");
	}
	//parses cond
	public void cond(){
		System.out.println(tabs.substring(0,numTabs++)+"<Cond>");
		id();
		operator();
		term();
		System.out.println(tabs.substring(0,--numTabs)+"</Cond>");
	}
	//parses term
	public void term(){
		System.out.println(tabs.substring(0,numTabs++)+"<Term>");
		if(token.getType() == Token.Type.ID){
			id();
		} else if(token.getType() == Token.Type.INT){
			intlit();
		} else if(token.getType() == Token.Type.FLOAT){
			floatlit();
		} else {
			System.out.println("Invalid Syntax: Expected ID, INT, or FLOAT found "+token.getValue());
			System.exit(1);
		}
		System.out.println(tabs.substring(0,--numTabs)+"</Term>");
	}
	//parses id. outputs id  terminal
	public void id(){
		check(Token.Type.ID);
		System.out.println(tabs.substring(0,numTabs)+"<Id>"+token.getValue()+"</Id>");
		token = lexer.nextToken();
	}
	//parses float literals. outputs float terminal
	public void floatlit(){
		check(Token.Type.FLOAT);
		System.out.println(tabs.substring(0,numTabs)+"<FLOAT>"+token.getValue()+"</FLOAT>");
		token = lexer.nextToken();
	}
	//parses int literals. outputs int terminal
	public void intlit(){
		check(Token.Type.INT);
		System.out.println(tabs.substring(0,numTabs)+"<Int>"+token.getValue()+"</Int>");
		token = lexer.nextToken();
	}
	//parses commas. outputs comma terminal
	public void comma(){
		check(Token.Type.COMMA);
		System.out.println(tabs.substring(0,numTabs)+"<Comma>"+token.getValue()+"</Comma>");
		token = lexer.nextToken();
	}
	//parses operators. outputs operator terminal
	public void operator(){
		check(Token.Type.OPERATOR);
		System.out.println(tabs.substring(0,numTabs)+"<Operator>"+token.getValue()+"</Operator>");
		token = lexer.nextToken();
	}
	//parses keywords
	public void keyword(Token.Type type){
		check(type);
		System.out.println(tabs.substring(0,numTabs)+"<keyword>"+token.getValue()+"</keyword>");
		token = lexer.nextToken();
	}

}
