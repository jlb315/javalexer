/* Jennifer Brown
 * jlb315
 * CSE 262
 * Program 1
 *
 * Java version 1.8
 * Compiled in Linux. makefile included. 
 */

/* Test class to test Parser */
public class Test {
	public static void main(String[] args){
		Parser p = new Parser("SELECT C1,C2 FROM T1 WHERE C1=5.23");
		p.run();

		Parser q = new Parser("SELECT C1 , C2 FROM XY WHERE C1>x AND C2<407");
		q.run();
	}
}
