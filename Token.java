/* Jennifer Brown
 * jlb315
 * CSE 262
 * Program 1
 *
 * Java version 1.8
 * Compiled on Linux. Makefile included.
 */

/* Class Token used to store the value and type of any token for limited
 * SQL grammar
 */

public class Token{
	public enum Type {INT, FLOAT, ID, SELECT, FROM, WHERE, AND, OPERATOR, COMMA, EOI, ERROR}
	private Type tokenType;
	private String value;
	//constructor
	public Token(Type type, String lexeme){
		this.tokenType = type;
		this.value = lexeme;
	}
	//returns tokenType
	public Type getType(){
		return tokenType;
	}
	//returns value of token
	public String getValue(){
		return value;
	}


}
